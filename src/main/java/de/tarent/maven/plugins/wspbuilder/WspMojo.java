//Maven WSPBuilder-Plugin
//The Plugin supports the build of wsp-builder-projects under windows with maven/npanday
//Copyright (C) 05.01.2011 tarent GmbH
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License,version 2
//as published by the Free Software Foundation.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//02110-1301, USA.
//
//tarent GmbH., hereby disclaims all copyright
//interest in the program 'IMSNeu'
//Signature of Elmar Geese, 05.05.2010
//Elmar Geese, CEO tarent GmbH.

package de.tarent.maven.plugins.wspbuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.maven.artifact.handler.manager.ArtifactHandlerManager;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

/**
 * Goal which builds a wsp
 *
 * @goal wsp
 * 
 * @phase package
 */
public class WspMojo
    extends AbstractMojo
{
	/**
     * Location of wsp-builder
     * @parameter
     * @required
     */
    private String pathToWspBuilder;
	
    /**
     * Location of feature.xml
     * @parameter
     * @required
     */
    private String pathToFeatureXml;
    
    /**
     * Location of the 12 hive
     * @parameter
     * @required
     */
    private String projectPath;
    
    /**
     * targetPath
     * @parameter
     * @required
     */
    private String targetPath;
        
    /**
     * Version String
     * @parameter expression="${project.version}"
     * @required
     */
    private String version;  
    
    /**
     * @parameter default-value="${project}"
     */
    public MavenProject mavenProject;
       
    /**
     * @component
     * @required
     * @readonly
     */ 
     public ArtifactHandlerManager artifactHandlerManager;
   

    
    public void execute()
        throws MojoExecutionException
    {   	
    	getLog().info("tarent-wspbuilder-plugin: successfully loaded");
    	
    	//update and check version number
    	updateVersionNumbers();
    	
    	//check if target path exists, create if not
    	if(!new File(targetPath).exists()){
    		new File(targetPath).mkdir();
    	}
    	
    	//build the wsp
    	callWspBuilder();
    	
    	//now switch to wsp and set the artifact to the wsp file
    	mavenProject.getArtifact().setArtifactHandler(artifactHandlerManager.getArtifactHandler("wsp"));
    	mavenProject.getArtifact().setFile(new File(targetPath + "\\" + mavenProject.getArtifactId() + ".wsp"));
    }
    
	private void callWspBuilder() throws MojoExecutionException {
		if(!pathToWspBuilder.endsWith("/") && !pathToWspBuilder.endsWith("\\")){
			pathToWspBuilder += "\\";
		}
		try{
			Process p =  Runtime.getRuntime().exec(pathToWspBuilder + "wspbuilder.exe -ProjectPath \"" + projectPath + "\" -OutputPath \"" + targetPath + "\"");
			InputStream stdout = p.getInputStream();
			BufferedReader brOut = new BufferedReader(new InputStreamReader(stdout));
			String s = "";
			while((s = brOut.readLine()) != null){
				getLog().info("tarent-wspbuilder-plugin: wspbuilder: " + s);
			}
			brOut.close();		
		}catch(Exception e){
			throw new MojoExecutionException("Error during wspbuilder-call", e);
		}
	}

	private void updateVersionNumbers() throws MojoExecutionException {
		String normalizedVersion = normalizeVersionNumber(version);
		File featurexml = new File(pathToFeatureXml);
    	if(!featurexml.isFile()) throw new MojoExecutionException("pathToFeatureXml invalid");
		try{
			DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dbuilder = dbfactory.newDocumentBuilder();
			Document featuredoc = dbuilder.parse(featurexml);
			Node rootNode = featuredoc.getDocumentElement();
			
			NamedNodeMap nnm  = rootNode.getAttributes();
			for(int j=0; j<nnm.getLength();j++){
				Node item = nnm.item(j);
				if("ReceiverAssembly".equals(item.getNodeName())){
					String[] splits = item.getNodeValue().split(",");
					String resultString = "";
					for(int i=0; i<splits.length; i++){
						if(splits[i].contains("Version=")) splits[i] = " Version=" + normalizedVersion;
						resultString += splits[i];
						if(i<splits.length-1) resultString+=",";
					}
					item.setNodeValue(resultString);
					getLog().info("tarent-wspbuilder-plugin: Receiver Assembly Version successfully changed");
				}
				else if("Version".equals(item.getNodeName())){
					item.setNodeValue(normalizedVersion);
					getLog().info("tarent-wspbuilder-plugin: Version successfully changed");
				}
			}
			XMLSerializer serializer = new XMLSerializer();
			FileWriter fw = new FileWriter(featurexml);
			serializer.setOutputCharStream(fw);
			serializer.serialize(featuredoc);
			fw.close();
		}
		catch(Exception e){
			throw new MojoExecutionException("problem parsing feature.xml", e);
		}
	}

	private String normalizeVersionNumber(String actualVersion) throws MojoExecutionException {
		String newVersion="";
		String[] exploded = actualVersion.split("\\.");
		for(int i=0;i<4;i++){
			if(i<exploded.length){
				try{
					Integer.parseInt(exploded[i]);
					newVersion+=exploded[i];
				}
				catch(NumberFormatException n){
					if(exploded[i].contains("-")){
						newVersion+=exploded[i].substring(0, exploded[i].indexOf("-")) + "." + "0";
						i++;
					}
					else{
						newVersion+="0";
					}
				}	
			}
			else{
				newVersion+="0";
			}
			if(i<3){
				newVersion+=".";
			}
		}
		if(!newVersion.equals(actualVersion)){
			getLog().warn("Version Number mismatch, changed: " + actualVersion + " to: " + newVersion 
					+ " if this is not the behaviour you expected, please respect the MS convention of int.int.int.int");
		}
		return newVersion;
	}	
}

